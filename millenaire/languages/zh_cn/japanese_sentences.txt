female.BecomeAdult=Watashi wa ituka hahaoya ni naritai desu. / 我想成为家庭主妇。

male.BecomeAdult=Watashi wa sensi ni naritai desu. /我想成为一个战士。

villager.BecomeAdult=Seityou suru jikan. / 是成长的时候了。

villager.BringBackResourcesHome=Cho cho cho... / 工作，工作，工作……

villager.ChopTrees=Watashi wa nanbonka no ki wo bassai suru tsumori da. / 我要去砍一些树。

villager.ConstructionStepByStep=Watashi no chichi wa kentiku no houhou ni tuite watashi ni oshieta. / 我的附近曾经教导过我如何建造。
villager.ConstructionStepByStep=Watashi wa kami ga watashi no kenchiku wo kiniiru koto wo negatte imasu. / 我希望神明喜欢我的建筑。

villager.DeliverGoodsHousold=Watashi wa mise kara syokuryou wo eta. / 我从市场买了些食物。

villager.GatherGoods=Watashi wa ikutuka no souchi ga hituyou da. / 我需要一些物资。

villager.GetGoodsHousold=Watashi wa ie ni korera no mono ga hituyou desu. / 我的家里需要这些东西。

villager.GetHouseResources=Watashi wa ie kara nanika totte konakereba ikenai. / 我要从家里拿一些东西。

villager.GetResourcesForBuild=Watashi wa motto takusan no zairyou ga hituyou desu. / 我需要更多材料。

villager.GoPlay=watashi wa asobitai desu. / 我想出去玩。

villager.childobserveagriculture=Watashinohaha wa,-mai sakumotsu o saibai suru koto wa jūyōdesuga, sore wa watashi no tame ni totemo taikutsuda to iimasu... / 我妈妈说培植水稻很重要，但对我来说很无聊。
villager.childobserveconstruction=Tabun watashi ga ōkiku naru to, watashi wa tō o taterudeshou. Kin kara! / 也许当我更大的时候，我会建造宝塔。黄金！
villager.childobservesmithing=Watashitachi no buki ya yoroi wa mottomo utsukushī monodesu! / 我们的武器和盔甲是最酷的！
villager.childobserveproducefood=Naze koko ni subete ga kome de tsukura renakereba naranai nodesu ka? Watashi wa kome ni tsukarete iru! / 为什么这里所有的东西都是用稻米做的？我讨厌吃稻米！
villager.childobserveproducealcohol=Zenkai watashi wa sake o tameshite mimashita. / 我上次喝酒的时候病了。

villager.childeatapple=Kono sekai wa mahōdesu! Ringo wa subete no ki ni sodatsu! / 这个世界太神奇了！苹果生长在每棵树上！

villager.MakeTools=Watashi wa naganen no aida saikoukyu no dougu wo koumyou ni tukutte iru. / 这些年里我做的工具都是最棒的。

villager.MakeWeapons=Saikoukyu no buki no himitu wa sisyou kara watashi ni uketugareta. / 我从师傅那里传承了制作出最好武器的技艺。

villager.PlantSaplings=Watashi wa korera no naegi wo ueru tumori desu. / 我要种这些树苗。

villager.SlaughterChicken=Anata wa dareka no tame ni subarashii shokuji wo tukuru darou. / 小鸡, 你会成为人们喜欢的优质肉类。

villager.calltoarms=Karera wa watashitachi wo kougeki suru hi wo koukai suru darou! / 他们终将会为攻击了我们而后悔！
villager.calltoarms=Kuki wo mote! / 拿上你的武器！
villager.calltoarms=Mura wo mamore! / 保卫村庄！

villager.gopray=Watashi wa housaku no tame ni megami Uga-no-Mintama ni inori masu. / 我要向女神Uga-no-Mintama祈求丰收。
villager.gopray=Kami wa watashitachi wo aku kara osukui ni naru. / 神明保佑我们远离邪恶。
villager.gopray.chosen=Kokoro wo otitukasete kudasai. / 放松你的思想。

villager.gorest=Watashi niwa kyu-kei ga hituyou desu. / 我需要休息。
villager.gorest=A-, mou neru jikan da / 啊, 我那美好的睡觉时间。
villager.gorest=Watashi wa totemo tukarete imasu./ 我太累了。

villager.greeting=Konnichiwa, $name-san! / 早上好， $name-桑!！
villager.greeting=Oi. / 喂。
villager.greeting=Ogenki desuka, $name-san  / 你好吗，$name-桑？
villager.greeting=Kawatta koto aru? / 有什么新消息吗？
villager.greeting=Hajimemashite / 见到你很高兴。
villager.greeting=Yokoso. / 欢迎。
villager.greeting=Gokigenyou. / 你好吗？
